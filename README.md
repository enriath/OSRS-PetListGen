# OldSchool RuneScape Pet List Generator

An Oldschool Runescape Pet List Generator, initially prototyped in Python 3, with a web-hosted JavaScript version following in its footsteps.

|The JavaScript version makes images like this:|Whereas the Python 3 version made images like this:|
|-----------|------------|
|![](http://i.imgur.com/hrinxXb.png)|![](http://i.imgur.com/rvV1z7o.png)|

## How to use?

Click [here](https://hydrox.io/osrs/petlist) to go to the online JavaScript version.

Legacy instructions for the Python 3 version can be found in [the Python3 Branch](https://gitlab.com/hydrox6/OSRS-PetListGen/tree/Python3)

### Found a bug?

Let me know via an issue. Please provide as much information possible when submitting an issue.

### Credits

/u/MiniYoyo05 for their brilliant [drop chart](https://i.redd.it/5zqs6vrmhf1y.png), which inspired the style of this project.